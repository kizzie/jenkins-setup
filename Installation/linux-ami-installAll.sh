#!/bin/bash

#update all
sudo yum update -y

#install wget if required
sudo yum install -y wget

#setup maven
wget http://apache.mirrors.ionfish.org/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.tar.gz
sudo tar xzf apache-maven-3.3.3-bin.tar.gz -C /usr/local
cd /usr/local
sudo ln -s apache-maven-3.3.3 maven
sudo ln -s maven/bin/mvn /usr/bin/mvn

#install git
sudo yum install -y git

#install and start jenkins
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo
rpm --import http://pkg.jenkins-ci.org/redhat-stable/jenkins-ci.org.key
sudo yum install -y jenkins
sudo service jenkins start
